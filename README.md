# sdk_v09.01_ast2600

#### 介绍
基于sdk_v09.01， 在芯片ast2600上porting使用。 参考代码提交。

#### 说明
* 1.分支ast2600_porting_base_network为基本的功能porting，可以让BMC成功运行到kernel并可以访问网络。
* 2.分支yuan_support_power_status为适配power status，显示系统开关机状态。
